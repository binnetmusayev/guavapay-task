package com.guavapay.ms.userservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    Integer id;
    String first_name;
    String last_name;
    @Enumerated(EnumType.STRING)
    Role role;
    String user_name;
    @JsonIgnore
    String password;

}