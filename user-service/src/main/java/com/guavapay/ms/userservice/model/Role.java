package com.guavapay.ms.userservice.model;

public enum Role {
    USER, ADMIN, COURIER;
}
