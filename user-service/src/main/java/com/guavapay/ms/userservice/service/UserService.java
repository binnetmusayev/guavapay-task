package com.guavapay.ms.userservice.service;

import com.guavapay.ms.userservice.model.Role;
import com.guavapay.ms.userservice.model.User;
import com.guavapay.ms.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public List<User> getAllCouriers() {
        return userRepository.findAll()
                .parallelStream()
                .filter(user -> user.getRole().equals(Role.COURIER))
                .collect(Collectors.toList());
    }

    public boolean isValidCourier(int courierId){
        User user = userRepository.findById(courierId).orElse(null);
        return user !=null && user.getRole().equals(Role.COURIER);
    }

    public void createCourier(User user){
        if (user.getRole().toString().equalsIgnoreCase(Role.COURIER.toString())){
            userRepository.save(user);
        }
    }

}
