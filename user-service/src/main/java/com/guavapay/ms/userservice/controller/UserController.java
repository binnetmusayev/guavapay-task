package com.guavapay.ms.userservice.controller;

import com.guavapay.ms.userservice.model.User;
import com.guavapay.ms.userservice.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class UserController {

    @Autowired
    private UserService service;

    @ApiOperation("Return list of all Users")
    @GetMapping("/users")
    public List<User> getAllUsers(){
        return service.getAllUsers();
    }

    @ApiOperation("Return list of all Couriers")
    @GetMapping("/couriers")
    public List<User>  getAllCouriers(){
        return service.getAllCouriers();
    }

    @ApiOperation("Checks validity of courierId")
    @GetMapping("/couriers/{courierId}")
    public boolean isValidCourier(@PathVariable int courierId){
        return service.isValidCourier(courierId);
    }

    @ApiOperation("Creates Courier")
    @PostMapping("/couriers")
    public void createCourierAccount(@RequestBody User courier){
        service.createCourier(courier);
    }
}
