package com.guavapay.ms.userservice.controller;

import com.guavapay.ms.userservice.model.JwtRequest;
import com.guavapay.ms.userservice.model.JwtResponse;
import com.guavapay.ms.userservice.model.User;
import com.guavapay.ms.userservice.service.UserService;
import com.guavapay.ms.userservice.utility.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class ApiController {

    @Autowired
    private UserService service;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JWTUtility jwtUtility;

    @PostMapping("/auth/register")
    public User register(@RequestBody User user){
        return service.register(user);
    }

    @PostMapping("/auth/signin")
    public JwtResponse signIn(@RequestBody JwtRequest jwtRequest){
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(jwtRequest.getUsername(), jwtRequest.getPassword())
        );
        UserDetails userDetails = service.loadUserByUsername(jwtRequest.getUsername());
        String token = jwtUtility.generateToken(userDetails);
        return new JwtResponse(token);
    }

}
