package com.guavapay.ms.userservice.service;

import com.guavapay.ms.userservice.model.JwtRequest;
import com.guavapay.ms.userservice.model.JwtResponse;
import com.guavapay.ms.userservice.model.User;
import com.guavapay.ms.userservice.model.Role;
import com.guavapay.ms.userservice.repository.UserRepository;
import com.guavapay.ms.userservice.utility.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUserName(username);
        return new org.springframework.security.core.userdetails.User(user.getUser_name(),
                user.getPassword(), new ArrayList<>());
    }

    public User register(User user){
        return userRepository.save(user);
    }
}
