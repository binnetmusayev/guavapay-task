package com.guavapay.ms.userservice.repository;

import com.guavapay.ms.userservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query("SELECT t FROM User t where t.user_name = ?1")
    User findByUserName(String userName);
}
