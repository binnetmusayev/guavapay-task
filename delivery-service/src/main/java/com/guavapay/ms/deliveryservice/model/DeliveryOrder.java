package com.guavapay.ms.deliveryservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name="orders")
public class DeliveryOrder {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    Integer id;
    @Enumerated(EnumType.STRING)
    private OrderStatus status;
    String destination;
    Integer coordinate_x;
    Integer coordinate_y;
    LocalDateTime creation_timestamp;
    Integer courier_id;
}
