package com.guavapay.ms.deliveryservice.service;

import com.guavapay.ms.deliveryservice.model.DeliveryOrder;
import com.guavapay.ms.deliveryservice.model.OrderStatus;
import com.guavapay.ms.deliveryservice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private RestTemplate restTemplate;

    public void createOrder(DeliveryOrder order){
        if (order.getCreation_timestamp()==null){
            order.setCreation_timestamp(LocalDateTime.now());
        }
        orderRepository.save(order);
    }

    public void changeDestination(int orderId, String destination){
        DeliveryOrder order = orderRepository.getById(orderId);
        order.setDestination(destination);
        orderRepository.save(order);
    }

    public void cancelOrder(int orderId){
        DeliveryOrder order = orderRepository.getById(orderId);
        order.setStatus(OrderStatus.CANCELLED);
        orderRepository.save(order);
    }

    public DeliveryOrder details(int orderId){
        return orderRepository.findById(orderId).orElse(null);
    }

    public List<DeliveryOrder> detailsOfAll(){
        return orderRepository.findAll();
    }

    public void changeStatus(int orderId, String status) throws Exception{
        boolean success = false;
        for (OrderStatus s : OrderStatus.values()) {
            if (s.name().equalsIgnoreCase(status)) {
                DeliveryOrder order = orderRepository.getById(orderId);
                order.setStatus(s);
                orderRepository.save(order);
                success = true;
            }
        }
        if(!success) throw new Exception("invalid status");
    }

    public void assign(int orderId, int courierId) throws Exception{
        DeliveryOrder order = orderRepository.getById(orderId);
        //fetching validity from User micro-service
        boolean isValidCourier = Boolean.TRUE.equals(
                restTemplate.getForObject("http://user-service/couriers/" + courierId, Boolean.class));
        if (isValidCourier) {
            order.setCourier_id(courierId);
            orderRepository.save(order);
        }else{
            throw new Exception("invalid courier id");
        }
    }

    public String getTracking(int orderId){
        DeliveryOrder order = orderRepository.getById(orderId);
        return "X: " +order.getCoordinate_x() + ", Y: " +order.getCoordinate_y();
    }

}
