package com.guavapay.ms.deliveryservice.controller;

import com.guavapay.ms.deliveryservice.model.DeliveryOrder;
import com.guavapay.ms.deliveryservice.service.OrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/")
public class DeliveryController {

    @Autowired
    private OrderService service;

    @ApiOperation("Create Delivery Order")
    @PostMapping("/order")
    public void createParcelDeliveryOrder(@RequestBody DeliveryOrder order){
        service.createOrder(order);
    }

    @ApiOperation("Updates destination of Delivery Order")
    @PutMapping("/order/{orderId}/{destination}")
    public void changeDestination(@PathVariable int orderId, @PathVariable String destination){
        service.changeDestination(orderId, destination);
    }

    @ApiOperation("Cancels Delivery Order")
    @PutMapping("cancel/{orderId}")
    public void cancelOrder(@PathVariable int orderId){
        service.cancelOrder(orderId);
    }

    @ApiOperation("Returns requested Delivery Order")
    @GetMapping("/details/{orderId}")
    public DeliveryOrder  details(@PathVariable int orderId){
        return service.details(orderId);
    }

    @ApiOperation("Returns all Delivery Orders")
    @GetMapping("/details")
    public List<DeliveryOrder> detailsOfAll(){
        return service.detailsOfAll();
    }

    @ApiOperation("Updates status of Delivery Order")
    @PutMapping("status/{orderId}/{status}")
    public void changeStatus(@PathVariable int orderId, @PathVariable String status) throws Exception{
        service.changeStatus(orderId, status);
    }

    @ApiOperation("Assigns Delivery Order to a courier")
    @PutMapping("assign/{orderId}/{courierId}")
    public void  assign(@PathVariable int orderId, @PathVariable int courierId) throws Exception{
        service.assign(orderId, courierId);
    }

    @ApiOperation("Returns coordinates of Delivery Order")
    @GetMapping("/track/{orderId}")
    public String trackOrder(@PathVariable int orderId){
        return service.getTracking(orderId);
    }


}
