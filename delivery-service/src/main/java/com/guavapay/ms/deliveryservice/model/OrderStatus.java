package com.guavapay.ms.deliveryservice.model;

public enum OrderStatus {
    CREATED, ASSIGNED, DELIVERED, CANCELLED;
}
