package com.guavapay.ms.deliveryservice.repository;

import com.guavapay.ms.deliveryservice.model.DeliveryOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<DeliveryOrder, Integer> {

}
